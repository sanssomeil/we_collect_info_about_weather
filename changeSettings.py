import json
import loadSettings
import searchLoc


def changeSettingsOfApp() -> None:
    """Изменяет настройки приложения"""
    try:
        settings_app = loadSettings.loadSettingsFromFile() # загрузка настроек из файла
        run_sett = 0
        while run_sett == 0:
            change_str = ''
            print('')
            print('1. Установить новую локацию по умолчанию.')
            print('2. Изменить API Key.')
            print('3. Вернуться в главное меню.')
            print('')
            choice = input('Выберите пункт: ')
            if choice == '1':
                search_res = searchLoc.searchLocationByName()  # поиск локации
                loc = int(input('Выберите нужную локацию: '))
                length_res = len(search_res[loc]) - 1
                for key in range(0,length_res):
                    change_str += search_res[loc][key+1]
                    if key is not length_res-1:  # чтобы строка не заканчивалась запятой
                        change_str += ', '
                print('Новая локация по умолчанию:', change_str)
                to_json = { "apikey": settings_app['apikey'],
                            "locationKey": search_res[loc][0],
                            "locationName": change_str }
                # запись изменённых настроек в файл
                with open('settings.json', 'w') as s:
                    s.write(json.dumps(to_json))
                print ('Настройки успешно сохранены в файл.')
                print ('')
            elif choice == '2':
                change_str = input('Введите новый API Key: ')
                to_json = { "apikey": change_str,
                            "locationKey": settings_app['locationKey'],
                            "locationName": settings_app['locationName'] }
                # запись изменённых настроек в файл
                with open('settings.json', 'w') as s:
                    s.write(json.dumps(to_json))
                print ('Настройки успешно сохранены в файл.')
                print ('')
            elif choice == '3':
                run_sett = 1
            else:
                check_act = input('Введенные данные неверны. Попробовать еще раз? [д/н]: ')
                if check_act == ' Д' or check_act == 'д':
                    pass
                else:
                    run_sett = 1

    except Exception as err:
        print('Что-то пошло не так со сменой настроек - ', str(err))
