import datetime
import xml.etree.ElementTree as ET


def createNewElementInXml(root_element: 'ElementTree', answer_for: 'json', answer_cc: 'json') -> None:
    """Создает новый элемент XML для записи в файл"""
    try:
        weather_info = ET.SubElement(root_element, 'WeatherInfo')  # создаем дочерний узел для корня
        date_of = ET.SubElement(weather_info, 'Date')  # создаем дочерние узлы для weather_info
        temperature_min = ET.SubElement(weather_info, 'MinimalTemperature')
        temperature_max = ET.SubElement(weather_info, 'MaximalTemperature')
        pressure = ET.SubElement(weather_info, 'AtmosphericPressure')
        description = ET.SubElement(weather_info, 'Description')
        d1 = datetime.datetime.strptime(answer_for['DailyForecasts'][0]['Date'], "%Y-%m-%dT%H:%M:%S%z")
        # присваем данные из запроса дочерним узлам для записи в файл
        date_of.text = d1.strftime('%d.%m.%Y')  # переводим дату в удобный формат
        temperature_min.text = str(answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Value']) + ' °' + answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Unit']
        temperature_max.text = str(answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Value']) + ' °' + answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Unit']
        pressure.text = str(round(answer_cc[0]['Pressure']['Metric']['Value'] * 0.75006375541921)) + ' мм рт.ст. ' + answer_cc[0]['PressureTendency']['LocalizedText']
        description.text = 'Днём: ' + answer_for['DailyForecasts'][0]['Day']['LongPhrase'] + '. Ночью: ' + answer_for['DailyForecasts'][0]['Night']['LongPhrase'] + '.'

    except Exception as err:
        print('Что-то пошло не так при создании нового элемента в XML - ', str(err))
