import loadSettings
import sendReqWeather
import searchLoc
import outputInfoFromReq
import writeInFile


def getInformationAboutAnotherLocation() -> None:
    """Выполняет поиск города и выводит информацию о погоде в нем. """
    try:
        url_forecast = 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/'
        url_current_conditions = 'http://dataservice.accuweather.com/currentconditions/v1/'
        settings_app = loadSettings.loadSettingsFromFile()  # загрузка настроек из файла
        search_res = searchLoc.searchLocationByName()  # поиск локации
        choice = int(input('Выберите нужную локацию: '))
        # запрос информации о погоде
        answer_for = sendReqWeather.sendRequestAboutWeatherInLocation(url_forecast, search_res[choice][0])
        answer_cc = sendReqWeather.sendRequestAboutWeatherInLocation(url_current_conditions, search_res[choice][0])
        outputInfoFromReq.outputInformationAboutWeather(answer_for, answer_cc)  # вывод полученных данных в консоль
        out = input('Хотите записать данные о погоде в файл? [д/н]: ')
        if out == 'Д' or out == 'д':
            writeInFile.writeInformationInFile(search_res[choice][0], answer_for, answer_cc)  # запись полученных данных в файл
            print('Данные записаны в файл.')
        else:
            pass

    except Exception as err:
        print('Что-то пошло не так с информацией о другой локации - ', str(err))
