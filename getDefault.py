import loadSettings
import sendReqWeather
import outputInfoFromReq
import writeInFile


def getInformationAboutDefaultLocation() -> None:
    """Работает с локацией, заданной по умолчанию."""
    try:
        url_forecast = 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/'
        url_current_conditions = 'http://dataservice.accuweather.com/currentconditions/v1/'
        settings_app = loadSettings.loadSettingsFromFile()  # загрузка настроек из файла
        locationKey = settings_app['locationKey']
        print('')
        print('Локация по умолчанию: ' + settings_app['locationName'])
        # запрос информации о погоде в локации
        answer_for = sendReqWeather.sendRequestAboutWeatherInLocation(url_forecast, locationKey)
        answer_cc = sendReqWeather.sendRequestAboutWeatherInLocation(url_current_conditions, locationKey)
        outputInfoFromReq.outputInformationAboutWeather(answer_for, answer_cc)  # вывод полученных данных в консоль
        writeInFile.writeInformationInFile(locationKey, answer_for, answer_cc)  # запись полученных данных в файл

    except Exception as err:
        print('Что-то пошло не так при работе с локацией по умолчанию - ', str(err))
