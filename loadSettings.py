import json
import changeSettings


def loadSettingsFromFile() -> dict:
    """Загружает настройки приложения из файла"""
    try:
        with open('settings.json', 'r') as s:  # открывает файл
            settings = json.load(s)
        return settings

    except FileNotFoundError:
        q = input('Файл настроек не найден. Перейти в настройки? [д/н]: ')
        if q == 'Д' or q == 'д':
            changeSettings.changeSettingsOfApp()
        else:
            pass

    except Exception as err:
        print('Что-то пошло не так - ', str(err))
