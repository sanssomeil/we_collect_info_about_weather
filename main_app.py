import getDefault
import outputInfoFromFile
import getAnother
import changeSettings

print('Привет!')
run_app = 0

while run_app == 0:
    # меню приложения
    print('')
    print('1. Получить последние данные о погоде в локации по умолчанию и сохранить их.')
    print('2. Прочитать все сохранённые данные о погоде в локации по умолчанию.')
    print('3. Получить последние данные о погоде в другой локации.')
    print('4. Изменить настройки.')
    print('5. Выход.')
    print('')

    sw_menu = input('Выберите пункт меню: ')
    if sw_menu == '1':
        getDefault.getInformationAboutDefaultLocation()
    elif sw_menu == '2':
        outputInfoFromFile.outputSavedInformation()
    elif sw_menu == '3':
        getAnother.getInformationAboutAnotherLocation()
    elif sw_menu == '4':
        changeSettings.changeSettingsOfApp()
    elif sw_menu == '5':
        run_app = 1
    else:
        check_act = input('Введенные данные неверны. Попробовать еще раз? [д/н]: ')
        if check_act == ' Д' or check_act == 'д':
            pass
        else:
            run_app = 1

print('')
print('До встречи!')
