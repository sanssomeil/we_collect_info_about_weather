import json
import loadSettings
import xml.etree.ElementTree as ET
from prettytable import PrettyTable


def outputSavedInformation() -> None:
    """Выводит в консоль информацию, сохраненную в файле"""
    settings_app = loadSettings.loadSettingsFromFile()  # загрузка настроек из файла
    try:
        locationKey = settings_app['locationKey']
        print('')
        print('Локация по умолчанию: ' + settings_app['locationName'])
        print('')
        th = ['Дата', 'Температура min', 'Температура max', 'Атмосферное давление', 'Описание']  # устанавливаем заголовки столбцов в таблице
        td = []
        # добавляем информацию в ячейки таблицы
        for event, elem in ET.iterparse(locationKey + '.xml'):
            if elem.text is not None:
                td.append(elem.text)
        columns = len(th)
        table = PrettyTable(th)
        # выводим таблицу
        while td:
            table.add_row(td[:columns])
            td = td[columns:]
        print(table)

    except FileNotFoundError:
        print('Не найден файл с данными о погоде. Вернитесь к первому пункту главного меню.')

    except Exception as err:
        print('Что-то пошло не так - ', str(err))
