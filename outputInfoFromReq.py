import datetime


def outputInformationAboutWeather(answer_for: 'json', answer_cc: 'json') -> None:
    """Выводит на экран информацию, полученную из запроса погоды."""
    try:
        d1 = datetime.datetime.strptime(answer_for['DailyForecasts'][0]['Date'], "%Y-%m-%dT%H:%M:%S%z")
        print('\nДата:', d1.strftime('%d.%m.%Y'))  # переводим дату в более удобный формат
        print('Минимальная температура:', str(answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Value']) + ' °' + answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Unit'])
        print('Максимальная температура:', str(answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Value']) + ' °' + answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Unit'])
        print('Атмосферное давление:', str(round(answer_cc[0]['Pressure']['Metric']['Value'] * 0.75006375541921)) + ' мм рт.ст. ' + answer_cc[0]['PressureTendency']['LocalizedText'])
        print('Описание: Днём: ' + answer_for['DailyForecasts'][0]['Day']['LongPhrase'] + '. Ночью: ' + answer_for['DailyForecasts'][0]['Night']['LongPhrase'] + '.')
        print('')

    except Exception as err:
        print('Что-то пошло не так при выводе полученной информации - ', str(err))
