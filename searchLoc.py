import requests
import loadSettings


def searchLocationByName() -> dict:
    """Выполняет поиск локации"""
    try:
        url_city = 'http://dataservice.accuweather.com/locations/v1/cities/autocomplete'
        settings_app = loadSettings.loadSettingsFromFile()  # загрузка настроек из файла
        run_search = 0
        while run_search == 0:
            query = input('Введите название населённого пункта: ')
            payload = {'apikey': settings_app['apikey'],
                       'q': query,
                       'language': 'ru'}  # параметры запроса
            response = requests.get(url_city, params=payload)  # запрос на поиск локации
            answer = response.json()
            search_result = {}
            i = 0
            # запись в список нужных данных из ответа сервера
            for key in answer:
                field_of = []
                field_of.append(answer[i]['Key'])
                field_of.append(answer[i]['LocalizedName'])
                field_of.append(answer[i]['AdministrativeArea']['LocalizedName'])
                field_of.append(answer[i]['Country']['LocalizedName'])
                search_result[i+1] = field_of
                i += 1
            print('')
            if len(search_result) != 0:
                print('Результаты поиска:')
                # вывод результатов поиска в консоль
                for key, value in search_result.items():
                    print(str(key) + '. ', end='')
                    for element in range(1, len(value)):  # не показываем первый элемент списка
                        print(value[element],  end='')
                        if element is not len(value)-1:  # чтобы строка не заканчивалась запятой
                            print(', ', end='')
                    print('')
                print('')
                run_search = 1  # локация найдена, повторно запускать поиск не надо
                return search_result
            else:
                print('Такая локация не найдена.')
                print('')

    except KeyError:
        print('Кажется, нужно сменить API key.')

    except Exception as err:
        print('Что-то пошло не так с поиском локации - ', str(err))
