import requests
import loadSettings


def sendRequestAboutWeatherInLocation(url: str, locationKey: str) -> 'json':
    """Запрашивает данные о погоде с сервера"""
    try:
        settings_app = loadSettings.loadSettingsFromFile()  # загрузка настроек из файла
        payload = {'apikey': settings_app['apikey'],
                   'language': 'ru',
                   'details': 'true',
                   'metric': 'true'}  # параметры запроса
        urlreq = url + locationKey
        response = requests.get(urlreq, params=payload)   # запрос информации о погоде
        answer = response.json()
        return answer

    except KeyError:
        print('Кажется, нужно сменить API key.')

    except Exception as err:
        print('Что-то пошло не так с запросом - ', str(err))
