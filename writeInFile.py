import os.path
import xml.etree.ElementTree as ET
import createInXml


def writeInformationInFile(file_name: 'str', answer_for: 'json', answer_cc: 'json') -> None:
    """Записывает информацию о погоде в файл"""
    try:
        if os.path.exists(file_name + '.xml'):  # проверка файла на существование, чтобы данные не перезаписывались
            tree = ET.parse(file_name + '.xml')  # чтение из файла
            root = tree.getroot()
            createInXml.createNewElementInXml(root, answer_for, answer_cc)  # создание нового дерева в XML
            tree.write(file_name + '.xml')  # запись в файл
        else:
            location_info = ET.Element('LocationInfo')
            createInXml.createNewElementInXml(location_info, answer_for, answer_cc)  # создание нового дерева в XML
            tree = ET.ElementTree(location_info)
            tree.write(file_name + '.xml')  # запись в файл
        print('Данные о погоде записаны в файл.')

    except Exception as err:
        print('Что-то пошло не так при записи информации в файл - ', str(err))
